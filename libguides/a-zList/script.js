<script> 
 
 function toggleMenu() {
     
     const navbarNav = document.querySelector('.navbar-nav');
     const navbarToggle = document.querySelector('.navbar-toggle');
     const closeToggle = document.querySelector('.close-icon');
     
     navbarNav.classList.toggle('show', !navbarNav.classList.contains('show'));
     navbarToggle.classList.toggle('hide', !navbarToggle.classList.contains('hide'));
     closeToggle.classList.toggle('show', !closeToggle.classList.contains('show'));
     if (closeToggle.classList.contains('show')) {
       closeToggle.style.display = 'block';
     } else {
       closeToggle.style.display = 'none';
     }

}

 </script> 
Within libguide, paste the code from both the html and css files. These files are built to store a backup of the syntax copied and pasted into the springshare app's look and feel (one for html and one for css/js)

### Initial Header and Footer Changes

1. Login
2. Under the main menu > ADMIN
    a. (for SANDBOX) -->  Click GROUPS
3. Select the edit icon ( ![Pencil](image.png "Edit Icon") ) from the "Retheming" row to make changes to this group.
4. Select the HEADER/FOOTER/TABS/BOXES tab
    a. Check "Exclude System Level Header"
    b. Select "Display this HTML"
    c. Paste code from /libguides/header.html
    d. Click SAVE HTML button
5. (Editting footer needed)
6. Expand TAB & BOX OPTIONS (below Group Footer)
    a. Under Tabs set fields:
        Shape:ROUND
        Active Background:	#febc11
        Active Font:	#3d4952
        Inactive Background:	#003360
        Inactive Font:	#ffffff

    b. Under Box set fields:
        Shape: ROUND		
        Border Width:	1
        Border: Ignore	
        Background: Ignore	
        Header Background:	#febc11
        Header Font:	#3d4952

    c. Under Guide Settings:
       Select radio -> Make this the default design for new guides but allow guide owners to customize their guides.
7. Select the CUSTOM JS/CSS CODE
    a. Paste code from /libguides/css_js.css (includes js code)
    b. Click SAVE
    c. Leave check boxes blank
8. Select LOOK & FEEL and LAYOUT tab
    a. Layout options for Homepage set to:
        2020 Homepage Redesign 
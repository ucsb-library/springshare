Within libcal, paste the code from both the html and css/js files. These files are built to store a backup of the syntax copied and pasted into the springshare app's look and feel (two for html and one for css/js). Only the Look & Feel tab is needed to customize the layout. Other tabs customize the content displayed.

### Initial Header and Footer Changes

1. Login and select LibCal
2. Under the main menu > ADMIN > Look & Feel
3. On the first tab (Look and Feel) under Public Page Layout
    a. Set content width to Fixed
    b. Click Save
4. Under Top Banner
    a. Leave Upload a New Banner unselected
5. Under Code Customizations - Public Pages
    a. Copy and paste css_js.css code into Custom JS/CSS Code
    b. Copy and paste header.html code into Custom Header Code
    c. Copy and paste footer.html code into Custom Footer Code
    d. Click SAVE button
6. Under Code Customizations - Table / Kiosk Pages
    a. Leave blank
7. Test the changes


Within libAnswers, paste the code from both the header & footer html and css_js files. These files are built to store a backup of the syntax copied and pasted into the springshare app's look and feel (two fields for html and one for css/js)

### Initial Header and Footer Changes

1. Login > Select libAnwers app from the main drop down
2. Under the main menu > ADMIN
    a. (for SANDBOX) -->  Click GROUPS
3. Select the edit icon ( ![Pencil](image.png "Edit Icon") ) from the "Retheming" row to make changes to this group.
4. Expand the LOOK and FEEL section.
5. Set GROUP BANNER to "Show no banner at all"
    a. Click SAVE
6. Set "No" to Include system-level JS/CSS code on this group's pages
    a. Click SAVE
7. Set "No" to Include system-level Header code on this group's pages
    a. Click SAVE
8. Set "No" to Include system-level Footer code on this group's pages
    a. Click SAVE
9. Add syntax to the CUSTOM CSS/JS field
    a. Paste code from /libAnswers/css_js.css (includes js code)
    b. Click SAVE
10. Add syntax to the CUSTOM HEADER field
    a. Check "Exclude System Level Header"
    b. Select "Display this HTML"
    c. Paste code from /libanswers/header.html
    d. Click SAVE button
5. Add syntax to the CUSTOM FOOTER field
    a. Paste code from /libanswers/css_js.css (includes js code)
    b. Click SAVE button

### FAQ Group pages

Within LibAnswers, a FAQ Group can be established that allows for additional pages to be created [https://ask.springshare.com/libanswers/faq/2385](https://ask.springshare.com/libanswers/faq/2385).

Group pages can have the overall look and feel applied. They also can have their own custom css and js;


1. Login into Springshare and select LibAnswers
2. Select from the main menu ADMIN > FAQ GROUPS
3. From the available list of FAQ GROUPS; locate the ACTIONS header, click on the box with pencil icon (EDIT GROUP SETTINGS)
4. Edit Look & Feel category
This project is for the Library's Springshare products.  Springshare is a vendor for several web-based systems that we use to provide various library services.  This project covers various tasks related to any of the Springshare produces.

Most documentation for Springshare is maintained on the Library Wiki.  For an overview of each product, see https://ucsb-atlas.atlassian.net/wiki/spaces/LIB/pages/17417798307/Springshare+Products.

This page includes information such as the product name, the primary functions for which it is used, the library stakeholders for that product, and the administrators for each product.

[LibAnswer's README](libanswers/README.md)

[LibCal's README](libcal/README.md)

[LibGuide's README](libguides/README.md)

